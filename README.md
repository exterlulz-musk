# README

musk is an adventure game engine using [Lua](http://www.lua.org/) and the [SDL](http://www.libsdl.org/). It's only available on Mac OS X for the moment, but will be available on Linux (and Windows if I can).

It will be a mix between [SCUMM](http://en.wikipedia.org/wiki/SCUMM) and [GrimE](http://en.wikipedia.org/wiki/GrimE) (but it will be written from scratch). It will allow people to write simple (and maybe complex) adventure games in the spirit of [Monkey Island](http://en.wikipedia.org/wiki/Monkey_Island_\(series\)) or [Maniac Mansion](http://en.wikipedia.org/wiki/Maniac_Mansion).

Don't be shy, post your bugs or suggestions in the [bug tracker](http://github.com/exterlulz/musk/issues).

Don't forget to look at [Grail](http://github.com/Droggelbecher/Grail) which is a project way more advanced than mine!

# Scripting example (not available yet)

    -- the configuration table
    CONFIG = {
      fps = 30,
      width  = 800,
      height = 600,
    }

# Links

- [home page](http://www.exterlulz.net/musk/)
- [wiki](http://github.com/exterlulz/musk/wiki)
- [bug tracker](http://github.com/exterlulz/musk/issues)
- [git mirror](http://repo.or.cz/w/exterlulz-musk.git)

# Licenses

## Lua

Copyright (c) 1994–2008 Lua.org, PUC-Rio.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
