project( MUSK )
# TODO: use MUSK_BINARY_DIR and MUSK_SOURCE_DIR

cmake_minimum_required( VERSION 2.8 )

# TODO: remove and add include( Lua51 )
include_directories( ./ )

set( CMAKE_CXX_FLAGS $ENV{CXXFLAGS} "-Wall" )

# TODO: load the SDL with Linux too...
if( APPLE )
  include( FindSDL )
  include( FindSDL_image )
  include( FindSDL_ttf )

  include_directories( ${SDL_INCLUDE_DIR} )
  include_directories( ${SDLIMAGE_INCLUDE_DIR} )
  include_directories( ${SDLTTF_INCLUDE_DIR} )
endif( APPLE )

file( GLOB MUSK_SOURCES
  main.cpp
  musk/*.cpp
  lua/*.c )

if( APPLE )
  list( APPEND MUSK_SOURCES SDLmain.m )
endif( APPLE )

# add: if( APPLE ) elseif( UNIX ) endif(...) for SDLmain

add_executable( musk MACOSX_BUNDLE ${MUSK_SOURCES} )
target_link_libraries( musk ${SDL_LIBRARY} ${SDLIMAGE_LIBRARY} )

# if( APPLE )
  # include( BundleUtilities )
  # install( DIRECTORY /Library/Frameworks/SDL.framework DESTINATION musk.app/Contents/Frameworks )
# endif( APPLE )
