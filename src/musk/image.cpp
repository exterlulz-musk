#include "image.h"

#include <SDL_image/SDL_image.h>

namespace musk {
  
  using std::string;
  using std::pair;
  using std::make_pair;
  
  Image::Image(const string& path) :
    surface_(0),
    clipMask_(),
    
    position_(0, 0) {
    
    surface_ = IMG_Load(path.c_str());
    
    clipMask_.x = 0;
    clipMask_.y = 0;
    clipMask_.w = width();
    clipMask_.h = height();
  }
  
  Image::Image(const Image& image) :
    surface_(0),
    clipMask_(image.clipMask_),
    position_(image.position_)
  {
    surface_ = SDL_ConvertSurface(image.surface_, image.surface_->format, image.surface_->flags);
  }
  
  Image::~Image()
  {
    SDL_FreeSurface(surface_);
    surface_ = 0;
  }
  
  int Image::width() const {
    return surface_->w;
  }
  
  int Image::height() const {
    return surface_->h;
  }
  
  int32_t Image::x() const {
    return position_.first;
  }
  
  int32_t Image::y() const {
    return position_.second;
  }
  
  void Image::setPosition(int32_t x, int32_t y) {
    position_ = make_pair<int32_t, int32_t>(x, y);
  }
  
  void Image::draw(SDL_Surface *dest) const
  {
    SDL_Rect dstrect = { position_.first, position_.second, 0, 0 };
    SDL_BlitSurface(surface_, const_cast<SDL_Rect *>(&clipMask_),
                    dest, &dstrect);
  }
  
} // namespace musk
