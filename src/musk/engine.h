#ifndef MUSK_ENGINE_H_
#define MUSK_ENGINE_H_

#include "lua_vm.h"
// ???: #include "scene.h"

#include <string>

struct SDL_Surface;

namespace musk {
  
  class Engine
  {
  public:
    Engine(const std::string& filename);
    ~Engine();
    
    void loadConfigFile(const std::string& filename);
    
    void boot();
    
    int fps() const;
    int width() const;
    int height() const;
    
    // TODO: remove, temporary
    SDL_Surface *screen() const;
    
  private:
    LuaVM lua_;
    
    int fps_;
    int width_, height_;
    
    SDL_Surface *screen_;
  };
  
} // namespace musk

#endif // MUSK_ENGINE_H_
