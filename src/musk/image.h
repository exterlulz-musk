#ifndef MUSK_IMAGE_H_
#define MUSK_IMAGE_H_

#include <SDL/SDL.h>

#include <string>
#include <utility>

namespace musk {
  
  class Image
  {
  public:
    Image(const std::string& path);
    Image(const Image& image);
    
    virtual ~Image();
    
    virtual int width() const;
    virtual int height() const;
    
    int32_t x() const;
    int32_t y() const;
    void setPosition(int32_t x, int32_t y);
    
    void draw(SDL_Surface *dest) const;
    
  protected:
    SDL_Surface *surface_;
    SDL_Rect clipMask_;
    
    std::pair<int32_t, int32_t> position_;
  };
  
} // namespace musk

#endif // MUSK_IMAGE_H_
