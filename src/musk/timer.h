#ifndef MUSK_TIMER_H_
#define MUSK_TIMER_H_

#include <stdint.h>

namespace musk {
  
  class Timer
  {
  public:
    Timer();
    
    uint32_t ticks() const;
    
    void reset();
  
  private:
    uint32_t ticks_;
  };
  
} // namespace musk

#endif // MUSK_TIMER_H_
