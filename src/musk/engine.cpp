#include "engine.h"

#include <SDL/SDL.h>
#include <SDL_image/SDL_image.h>

namespace musk {
  
  using std::string;
  
  Engine::Engine(const string& filename) :
    lua_(),
    width_(0), height_(0),
    screen_(0)
  {
    // TODO: init a smaller part of the SDL
    SDL_Init(SDL_INIT_EVERYTHING);
    IMG_Init(IMG_INIT_PNG);
    
    loadConfigFile(filename);
    
    screen_ = SDL_SetVideoMode(width_, height_, 32,
      SDL_SWSURFACE |
      SDL_HWSURFACE | SDL_DOUBLEBUF);
    // TODO: if screen_ == 0 ...
    SDL_WM_SetCaption("musk", 0);
  }
  
  Engine::~Engine()
  {
    IMG_Quit();
    SDL_Quit();
  }
  
  void Engine::loadConfigFile(const string& filename)
  {
    lua_.doFile(filename);
    
    fps_    = lua_.globalIntegerInTable("CONFIG", "fps");
    width_  = lua_.globalIntegerInTable("CONFIG", "width");
    height_ = lua_.globalIntegerInTable("CONFIG", "height");
  }
  
  void Engine::boot() {
    lua_.callFunction("BOOT");
  }
  
  int Engine::fps() const {
    return fps_;
  }
  
  int Engine::width() const {
    return width_;
  }
  
  int Engine::height() const {
    return height_;
  }
  
  // TODO: remove, temporary
  SDL_Surface *Engine::screen() const {
    return screen_;
  }
  
} // namespace musk
