#include "timer.h"

#include <SDL/SDL.h>

namespace musk {
  
  Timer::Timer() :
    ticks_(SDL_GetTicks()) {
  }
  
  void Timer::reset() {
    ticks_ = SDL_GetTicks();
  }
  
  uint32_t Timer::ticks() const {
    return SDL_GetTicks() - ticks_;
  }
  
} // namespace musk
