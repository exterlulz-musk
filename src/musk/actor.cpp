#include "actor.h"

namespace musk {
  
  Actor::Actor(const Animation& animation) :
    Animation(animation),
    isMoving_(true),
    movingDirection_(kDirectionRight),
    // TODO: change back speed_ and destination_ as (0, 0)
    speed_(1, 1),
    destination_(120, 120) {
  }
  
  void Actor::move()
  {
    if (!shouldMove()) {
      return;
    }
    
    setPosition(x() + speed_.first, y() + speed_.second);
  }
  
  bool Actor::shouldMove() const
  {
    if (!isMoving_) {
      return false;
    }
    
    if (x() == destination_.first &&
        y() == destination_.second) {
      return false;
    }
    
    return true;
  }
}
