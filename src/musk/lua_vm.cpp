#include "lua_vm.h"

#include "lua/lua.hpp"

// #include <cstring>
// #include <CoreFoundation/CoreFoundation.h>

namespace musk {
  
  using std::string;
  
  LuaVM::LuaVM() :
    L_(luaL_newstate())
  {
    // TODO: exit if newstate can't be created
    
    // TODO: open a smaller set of the libs?
    luaL_openlibs(L_);

    // TODO: replace with ResourcesManager somehow
    /* TODO: use when resources are placed inside the app folder
    
    // !!!: dirty hack to add the resource path to package.path in Lua
    // http://github.com/cinder/Cinder/blob/12e2ef3ec1bb92282d7a5b25ff143c68c85d40d7/src/cinder/app/App.cpp#L226
    char path[4096];
    
    CFURLRef url = ::CFBundleCopyResourcesDirectoryURL( ::CFBundleGetMainBundle() );
    ::CFURLGetFileSystemRepresentation( url, true, (UInt8*)path, 4096 );
    ::CFRelease( url );

    strlcat(path, "/?.lua;", 4096);
    
    // get the value of package.path
    lua_getglobal(L_, "package");
    lua_getfield(L_, -1, "path");
    const char *packagePath = luaL_checkstring(L_, -1);

    // append the package.path
    if (packagePath != NULL) {
      strlcat(path, packagePath, 4096);

      // pop the package.path value
      lua_pop(L_, 1);
    }
    
    lua_pushstring(L_, path);
    lua_setfield(L_, -2, "path");

    // pop the package table
    lua_pop(L_, 1);
    */
  }
  
  LuaVM::~LuaVM()
  {
    lua_close(L_);
    L_ = 0;
  }
  
  void LuaVM::doFile(const string& filename) {
    luaL_dofile(L_, filename.c_str());
  }
  
  int LuaVM::globalIntegerInTable(const string& table, const string& variable) const
  {
    // get table
    lua_getglobal(L_, table.c_str());
    
    // get table.variable
    lua_getfield(L_, -1, variable.c_str());
    int i = luaL_checkinteger(L_, -1);
    lua_pop(L_, 1);
    
    // pop the table
    lua_pop(L_, 1);
    return i;
  }
  
  void LuaVM::callFunction(const string& f)
  {
    lua_getglobal(L_, f.c_str());
    lua_call(L_, 0, 0);
  }
  
} // namespace musk
