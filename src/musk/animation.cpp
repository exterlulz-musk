#include "animation.h"

namespace musk {
  
  using std::string;
  
  Animation::Animation(const string& path, size_t frames, uint32_t durations) :
    Image(path),
    
    frames_(frames),
    durations_(),
    stopped_(false),
    
    currentFrame_(0),
    currentTicks_(0),
    
    width_(0)
  {
    if (frames_ > 1) {
      width_ = Image::width() / frames_;
    }
    else {
      frames_ = 1;
      width_ = Image::width();
    }
    
    clipMask_.w = width_;
    durations_.assign(frames_, durations);
  }
  
  int Animation::width() const {
    return width_;
  }
  
  uint32_t Animation::durationForFrame(size_t n) const {
    return durations_.at(n);
  }
  
  void Animation::start() {
    stopped_ = false;
  }
  
  void Animation::stop() {
    stopped_ = true;
  }

  void Animation::reset()
  {
    clipMask_.x = 0;
    
    currentFrame_ = 0;
    currentTicks_ = 0;
  }
  
  // TODO: separate in 2 methods: *shouldStep*, and the actual *step*
  void Animation::step(uint32_t ticks)
  {
    if (stopped_ == true) {
      return;
    }
    
    currentTicks_ += ticks;
    if (currentTicks_ < durationForFrame(currentFrame_)) {
      return;
    }
    currentTicks_ -= durationForFrame(currentFrame_);
    currentFrame_ = (currentFrame_ + 1) % frames_;
    
    clipMask_.x += width();
    if (clipMask_.x >= Image::width()) {
      reset();
    }
  }
  
} // namespace musk
