#ifndef MUSK_ANIMATION_H_
#define MUSK_ANIMATION_H_

#include "image.h"

#include <string>
#include <vector>

namespace musk {
  
  class Animation : public Image
  {
  public:
    // TODO: doc: duration is the duration of each frame (in ms)
    Animation(const std::string& path,
              size_t frames = 1, uint32_t durations = 250);
    
    virtual int width() const;
    
    uint32_t durationForFrame(size_t n) const;
    
    void start();
    void stop();
    
    void reset();
    void step(uint32_t ticks);
    
  private:
    size_t frames_;
    std::vector<uint32_t> durations_;
    bool stopped_;
    
    size_t currentFrame_;
    uint32_t currentTicks_;
    
    int width_;
  };
  
} // namespace musk

#endif // MUSK_ANIMATION_H_
