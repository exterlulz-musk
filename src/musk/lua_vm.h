#ifndef MUSK_LUA_VM_H_
#define MUSK_LUA_VM_H_

struct lua_State;

#include <string>

namespace musk {
  
  class LuaVM
  {
  public:
    LuaVM();
    ~LuaVM();
    
    void doFile(const std::string& filename);
    
    int globalIntegerInTable(const std::string& table, const std::string& variable) const;
    void callFunction(const std::string& f);
    
  private:
    lua_State *L_;
  };
  
} // namespace musk

#endif // MUSK_LUA_VM_H_
