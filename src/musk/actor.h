#ifndef MUSK_ACTOR_H_
#define MUSK_ACTOR_H_

#include "animation.h"

#include <utility>
#include <stdint.h>

namespace musk {
  
  // TODO: move to some musk.h with the position as pair<int, int>
  enum Direction {
    kDirectionLeft,
    kDirectionRight,
  };
  
  class Actor : public Animation
  {
  public:
    Actor(const Animation& animation);
    
    void move();
    // TODO: move as private?
    bool shouldMove() const;
  
  private:
    bool isMoving_;
    Direction movingDirection_;
    std::pair<int32_t, int32_t> speed_;
    std::pair<int32_t, int32_t> destination_;
  };
  
}

#endif // MUSK_ACTOR_H_
