#include "scene.h"

#include <SDL/SDL.h>

namespace musk {
  
  using std::list;
  
  Scene::Scene(const Image& background) :
    background_(background),
    animations_() {
  }
  
  void Scene::setBackground(const Image& background) {
    background_ = background;
  }
  
  void Scene::addAnimation(const Animation& animation) {
    animations_.push_back(animation);
  }
  
  void Scene::addActor(const Actor& actor) {
    actors_.push_back(actor);
  }
  
  void Scene::draw(SDL_Surface *dest)
  {
    background_.draw(dest);
    
    // draw the animations
    for (list<Animation>::iterator iter = animations_.begin();
         iter != animations_.end();
         iter++) {
      Animation& animation = *iter;
      animation.draw(dest);
    }
    
    // draw the actors
    for (list<Actor>::iterator iter = actors_.begin();
         iter != actors_.end();
         iter++) {
      Actor& actor = *iter;
      
      actor.move();
      actor.draw(dest);
    }
    
    SDL_Flip(dest);
  }
  
  void Scene::step(uint32_t ticks)
  {
    for (list<Animation>::iterator iter = animations_.begin();
         iter != animations_.end();
         iter++) {
      Animation& animation = *iter;
      animation.step(ticks);
    }
    
    // draw the actors
    for (list<Actor>::iterator iter = actors_.begin();
         iter != actors_.end();
         iter++) {
      Actor& actor = *iter;
      actor.step(ticks);
    }
  }
} // namespace musk
