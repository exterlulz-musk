#ifndef MUSK_SCENE_H_
#define MUSK_SCENE_H_

#include "image.h"
#include "animation.h"
#include "actor.h"

#include <list>

struct SDL_Surface;

namespace musk {
  
  class Scene
  {
  public:
    Scene(const Image& background);
    
    void setBackground(const Image& background);
    void addAnimation(const Animation& animation);
    void addActor(const Actor& actor);
    
    void draw(SDL_Surface *dest);
    void step(uint32_t ticks);
    
  private:
    Image background_;
    std::list<Animation> animations_;
    std::list<Actor> actors_;
  };
  
} // namespace musk

#endif // MUSK_SCENE_H_
