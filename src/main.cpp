#include <SDL/SDL.h>

#include "musk/engine.h"
#include "musk/scene.h"
#include "musk/image.h"
#include "musk/animation.h"
#include "musk/timer.h"

// TODO: Find a way to copy the SDL framework AND the resources

int main(int argc, char **argv)
{
  using musk::Engine;
  using musk::Scene;
  using musk::Image;
  using musk::Animation;
  using musk::Actor;
  using musk::Timer;
  
  Engine engine("../resources/main.lua");
  engine.boot();
  
  Scene scene(Image("../resources/background.png"));
  
  Actor hero(Animation("../resources/hero.png", 2));
  hero.setPosition(4, 24);
  scene.addActor(hero);
  
  uint32_t fps = engine.fps();
  Timer timer;
  
  bool quit = false;
  SDL_Event event;
  
  while (quit == false) {
    timer.reset();

    // TODO: let the engine handle the events
    while (SDL_PollEvent(&event) == 1) {
      if (event.type == SDL_QUIT) {
        quit = true;
        break;
      }
    }
    if (quit == true) {
      break; // TODO: or save the game?
    }
    
    scene.draw(engine.screen());
    
    if (timer.ticks() < (1000.0 / fps)) {
      SDL_Delay((1000.0 / fps) - timer.ticks());
    }
    
    scene.step(timer.ticks());
  }
  
  return 0;
}
